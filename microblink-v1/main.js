/***********************************************
 * MICROBLINK
 **********************************************/

// 1. initialize Firebase application before Microblink SDK to enable feature desktop-to-mobile
  // var config = {
  // apiKey: "AIzaSyC2EeRiAwTGj4Gv28RVjYLkU_sUYLafCqg",
  // authDomain: "microblink-api-exchanger.firebaseapp.com"
  //   databaseURL: "https://microblink-api-exchanger.firebaseio.com",
  //   projectId: "microblink-api-exchanger",
  // storageBucket: "discover-lab-dec-page",
  //   messagingSenderId: "902458803135"
  // };
  // firebase.initializeApp(config);
  // firebase.firestore().settings({});
  
  // 2. configure Microblink SDK
  
  // 2.1. set endpoint
  // 2.1.1. if not set default value is `https://api.microblink.com`
   Microblink.SDK.SetEndpoint('http://35.174.86.253:8080');
  // 2.1.2. for self-hosted on-premise microblink/api Docker Image available at Docker Hub
  // https://hub.docker.com/r/microblink/api
  // it is required to set endpoint where Microblink API is available
  // Microblink.SDK.SetEndpoint('http://localhost:8081');
  // Microblink.SDK.SetEndpoint('https://wt-f2bc1e742928378c2eda881f7dd00276-0.sandbox.auth0-extend.com/microblinkApiProxyExample');
  
  // 2.2. set recognizers (more recognizer -> slower response)
  // available recognizers at `https://api.microblink.com/recognize/info` at `data.recognizer.available`
  Microblink.SDK.SetRecognizers(['BLINK_CARD_FRONT']); 
  
  // 2.3. disable persisting uploaded data (this option is ignored if Authorization header is set)
  Microblink.SDK.SetIsDataPersistingEnabled(false);
  
  // 2.4. set Authorization header (Bearer + ' ' + apiKey + apiSecret) - generate one in Microblink dashboard at https://microblink.com/customer/api
  // if endpoint is not equal to the default value `https://api.microblink.com` then `Authorization` header is ignored (self-hosted Microblink API by default has disabled authorization with Microblink API key/secret)
  Microblink.SDK.SetAuthorization('Bearer OTgzMGE95GU0YTA1NGQ1OTk5OTVhZjk3OTJiNjM0YzE6NWMyMzhiNzktYmE5My00MTMzLWFiNTHUMWRhYTk4NWI5Mzcz');
  
  // 2.5. setup export images
  Microblink.SDK.SetExportImages(true);
  
  // 2.6. default listeners (API's callbacks) are defined inside of the microblink-js UI component, but it is possible to configure global listeners to implement custom success/error handlers
  Microblink.SDK.RegisterListener({
    onScanSuccess: (data) => {
      let results = data.result.data instanceof Array ? data.result.data : [data.result.data];
      // console.log(results[0].result)
      for (let i = 0; i < results.length; i++) {
        if (results[i].result == null) {
          results.splice(i, 1);
        }
      }
      if (results.length < 1) {
        $('.modal-body').html('<p>Scanning is finished, but we could not extract the data. Please check if you uploaded the right document type.</p>');
      } else {
        $('.modal-body').html(colorJson(results));
      }
      $('.modal-title').text("Scan success");
      //$('.modal').modal('show');
    },
    onScanError: (error) => {
      $('.modal-title').text("Error occured");
      $('.modal-body').html(colorJson(error));
      $('.modal').modal('show');
    }
  });

//   resultReady
//2.7. This event is fired after successful image upload or scan and the results are available on the client side. By listening to this event you are able for example to fill out your web form with the extracted data.
  document.querySelector('microblink-ui-web').addEventListener('resultReady', function(event) {
    const result = event.detail.result;
    const creditCardNumber = result.data[0].result.cardNumber.split(' ').join('');
    console.log(result.data[0])
    document.getElementById("cardOwner").value = result.data[0].result.owner;
    replaceFirst12(creditCardNumber);
    getCardType(creditCardNumber);
    document.getElementById("expMonth").value = result.data[0].result.validThru.month;
    let str = result.data[0].result.validThru.originalString;
    document.getElementById("expYear").value = str.substring(3);
    // document.getElementById("img_contain").innerHTML = `<img id="cardImage" src='data:image/jpeg;base64, ${result.data[0].result.fullDocumentImageBase64}'>`
    // convertBase64ToBlob(result.data[0].result.fullDocumentImageBase64)
  });

//   function convertBase64ToBlob(imageBase64){
//   fetch("data:image/jpeg;base64," + imageBase64)
//   .then(res => res.blob())
//   .then(blob => {
//     console.log(URL.createObjectURL(blob));
    
//   });
// }

let replaceFirst12 = (str) =>{
  document.getElementById("hiddenNumber").value = str.replace(/^.{1,12}/, m=> "*".repeat(m.length))
  document.getElementById("cardNo").value = str;
}

function hideCardValue(element){
  const tmpval = element.value;
  if(tmpval.length < 12){
    document.getElementById("cardNo").value += tmpval.replace(new RegExp('\\*', 'g'),"");
    document.getElementById("hiddenNumber").value = tmpval.replace(/\d{1}/,"*");
  }
}
  
  // 3. better UI, this removes microblink-js flash effect because microblink-js UI component (like any other HTML5 web component) loads asynchronusly and component's custom slots will look ugly until it's CSS/JS is loaded propertly (rendering speed depends on the CPU power of the machine where browser is used, 1 second if enough to cover any average machine).
  setTimeout(function () {
    document.querySelectorAll('.hide-until-component-is-loaded').forEach(function(element) {
      element.classList.remove('hide-until-component-is-loaded');
    })
  }, 1000)

  // 4. Credit Card Type
  function getCardType(number)
{
    // Visa
    let re = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
    if (number.match(re) != null){
        document.getElementById("cardType").innerHTML = '<i class="fa fa-cc-visa fa-2x mx-1"></i>'
    }

    // Mastercard
    re = new RegExp("^5[1-5][0-9]{14}$");
    if (number.match(re) != null){
        document.getElementById("cardType").innerHTML = '<i class="fa fa-cc-mastercard fa-2x mx-1"></i>'
    }

    // AMEX
    re = new RegExp("^3[47][0-9]{0,}$");
    if (number.match(re) != null){
      document.getElementById("cardType").innerHTML = '<i class="fa fa-cc-amex fa-2x mx-1"></i>'
    }

    // Discover
    re = new RegExp("^6(?:011|5[0-9]{2})[0-9]{12}$");
    if (number.match(re) != null){
      document.getElementById("cardType").innerHTML = '<i class="fa fa-cc-discover fa-2x mx-1"></i>'
    }

    // Diners
    re = new RegExp("^3(?:0[0-5]|[68][0-9])[0-9]{11}$");
    if (number.match(re) != null){
      document.getElementById("cardType").innerHTML = '<i class="fa fa-cc-diners-club fa-2x mx-1"></i>'
    }

    // Visa Electron
    re = new RegExp("^(4026|417500|4405|4508|4844|4913|4917)\d+$");
    if (number.match(re) != null){
      document.getElementById("cardType").innerHTML = '<i class="fa fa-cc-visa fa-2x mx-1"></i>'
    }

    return "";
}